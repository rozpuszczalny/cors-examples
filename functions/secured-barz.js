exports.handler = function (event, context, callback) {
    const auth = event.headers["authorization"] && event.headers["authorization"].split("Basic ")[1];
    const unauthorized = () => {
        callback(null, {
            statusCode: 401,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify({ "error": "No valid authorization header found" })
        });
    };
    if (!auth) {
        return unauthorized();
    }
    const [username, password] = Buffer.from(auth, "base64").toString('utf-8').split(":");
    if (username !== "admin" || password !== "admin") {
        return unauthorized();
    }
    callback(null, {
        statusCode: 200,
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({ "barz": [{ "name": "first" }] })
    });
};
