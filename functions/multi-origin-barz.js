exports.handler = function (event, context, callback) {
  const corsHeaders = {
    "Access-Control-Allow-Origin": "https://foo.rozpuszczalny.com",
    "Access-Control-Allow-Methods": "GET",
  };
  if (event.httpMethod === "OPTIONS") {
    return callback(null, {
      statusCode: 200,
      headers: {
        ...corsHeaders,
      },
    });
  }

  callback(null, {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json",
      ...corsHeaders,
    },
    body: JSON.stringify({ barz: [{ name: "first" }] }),
  });
};
