exports.handler = function (event, context, callback) {
    callback(null, {
        statusCode: 200,
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({ "barz": [{ "name": "first" }] })
    });
};
