exports.handler = function (event, context, callback) {
  callback(null, {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Expose-Headers": "Rate-Limit",
      "Rate-Limit": "4/10",
    },
    body: JSON.stringify({ barz: [{ name: "first" }] }),
  });
};
