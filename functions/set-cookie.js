exports.handler = function (event, context, callback) {
    callback(null, {
        statusCode: 204,
        headers: {
            "Set-Cookie": "super-secure=allow; domain=.rozpuszczalny.com; secure; httponly; max-age=3600; path=/",
            "Access-Control-Allow-Origin": "https://foo.rozpuszczalny.com",
            "Access-Control-Allow-Credentials": "true"
        }
    });
};
