const allowedOrigins = [
  "https://foo.rozpuszczalny.com",
  "https://boo.rozpuszczalny.com",
];

exports.handler = function (event, context, callback) {
  const corsHeaders = {
    "Access-Control-Allow-Methods": "GET",
    "Access-Control-Allow-Credentials": "true",
  };
  if (allowedOrigins.includes(event.headers["origin"])) {
    corsHeaders["Access-Control-Allow-Origin"] = event.headers["origin"];
  }
  if (event.httpMethod === "OPTIONS") {
    return callback(null, {
      statusCode: 200,
      headers: {
        ...corsHeaders,
      },
    });
  }

  callback(null, {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json",
      ...corsHeaders,
    },
    body: JSON.stringify({ barz: [{ name: "first" }] }),
  });
};
