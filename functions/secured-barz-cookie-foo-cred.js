exports.handler = function (event, context, callback) {
    const corsHeaders = {
        "Access-Control-Allow-Origin": "https://foo.rozpuszczalny.com",
        "Access-Control-Allow-Methods": "GET",
        "ACcess-Control-Allow-Credentials": "true"
    }
    if (event.httpMethod === "OPTIONS") {
        return callback(null, {
            statusCode: 200,
            headers: {
                ...corsHeaders
            }
        })
    }
    const auth = event.headers["cookie"];
    const unauthorized = () => {
        callback(null, {
            statusCode: 401,
            headers: {
                "Content-Type": "application/json",
                ...corsHeaders
            },
            body: JSON.stringify({ "error": "No valid authorization header found" })
        });
    };
    if (!auth) {
        return unauthorized();
    }

    if (auth.indexOf("super-secure=allow") !== 0) {
        return unauthorized();
    }
    callback(null, {
        statusCode: 200,
        headers: {
            "Content-Type": "application/json",
            ...corsHeaders
        },
        body: JSON.stringify({ "barz": [{ "name": "first" }] })
    });
};
